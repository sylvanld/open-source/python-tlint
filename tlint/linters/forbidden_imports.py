import ast
from distutils.log import INFO
import sys
from collections import namedtuple
from typing import List
from tlint.configuration import ModuleAccessConfiguration, ModuleAccessPolicy
from tlint.linters.linter_interface import ILinter

from tlint.loader import ModuleInfo

ImportInfo = namedtuple("Import", ["module", "name", "fqon", "alias"])

INFO = "\033[92mINFO \033[00m"
ERROR = "\033[91mERROR\033[00m"

def console_title(title: str):
    print('\n', "=" * 3, title, '='*(98-len(title)))

class ForbiddenImportsLinter(ILinter):
    def __init__(self, protected_modules: List[ModuleAccessConfiguration]):
        self.protected_modules = protected_modules
        
    @staticmethod
    def __iter_imports(path: str):
        with open(path) as fh:        
            root = ast.parse(fh.read(), path)

        for node in ast.iter_child_nodes(root):
            if isinstance(node, ast.Import):
                module = []
            elif isinstance(node, ast.ImportFrom):  
                module = node.module.split('.')
            else:
                continue

            for n in node.names:
                module_tree = n.name.split('.')
                yield ImportInfo(module, module_tree, '.'.join(module+module_tree), n.asname)

    def can_import(self, importer_module: str, imported_module: str):
        default_authorization = True

        for protected_module in self.protected_modules:
            if imported_module.startswith(protected_module.pattern): # TODO: add . correctly
                if protected_module.policy is ModuleAccessPolicy.GRANT:
                    exceptions = protected_module.forbidden
                else:
                    default_authorization = False
                    exceptions = protected_module.authorized
                
                for exception in exceptions:
                    if importer_module.startswith(exception):
                        return not default_authorization
            
        return default_authorization


    def lint_module(self, module_info: ModuleInfo):
        errors_counter = 0
        console_title('checking forbidden imports in ' + module_info.module_name)
        for import_info in self.__iter_imports(module_info.absolute_path):
            can_import = self.can_import(module_info.module_name, import_info.fqon)
            if not can_import:
                errors_counter += 1
            print(' '*4, INFO if can_import else ERROR,'---', module_info.module_name, 'can' if can_import else 'cannot', 'import', import_info.fqon)
        
        return errors_counter == 0
