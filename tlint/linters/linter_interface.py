import abc
from tlint.loader import ModuleInfo


class ILinter(abc.ABC):
    @abc.abstractmethod
    def lint_module(self, module_info: ModuleInfo):
        raise NotImplementedError
